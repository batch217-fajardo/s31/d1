const http = require("http");

const port = 4000; 

const server = http.createServer((request, response) => {

	// first endpoint
	if (request.url == "/greeting"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Hello Again!");
	} else if (request.url == "/homepage"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("This is the homepage.");
	} else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("ERROR: 404 - Page not found");	
	}

});

server.listen(port);

console.log(`Console now accessible at localhost: ${port}.`);