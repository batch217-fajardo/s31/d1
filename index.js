// require directive - load a particular module node.js

//http - hyper text transfer protocol
//http is a node module/application
//it consist of different components 
let http = require("http"); 

let port = 3000;

http.createServer(function (request, response){
	// writeHead
	// code - 200 - OK, No Problem, Success
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World!");
}).listen(port);

console.log(`Server is running at local host: ${port}`);